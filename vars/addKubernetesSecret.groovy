def add_kubernetes_secret(String secretName, String secretValue) {
    def addSecret = "kubectl create secret generic ${secretName} --from-literal=secret='${secretValue}' --dry-run -o json | kubectl apply -f -"
    sh(addSecret)
}