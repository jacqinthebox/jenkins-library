import groovy.text.SimpleTemplateEngine

def call(String inputFile, Map bindings) {
    def result = new SimpleTemplateEngine().createTemplate(inputFile).make(bindings)
    def stringResult = result.toString()
    result = null
    return stringResult
}
